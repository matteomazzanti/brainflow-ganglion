# Brainflow Sandbox
  
A scaffolded project to immediately start developing brainflow (java) applications,
without bothering about the project scaffolding.  
Setup your development environment and start coding!  

Developed on: 
- linux ubuntu 20 
- java
- gradle, groovy
- vscode
- brainflow 4.9.0

## Development setup

### Setup java environment
```
# List all java versions:
$ update-java-alternatives --list  
```
The requirement is: Java Runtime 55 = Java 11  
If this is not your case, perform the following:  

1. set the version for java
```
    $ sudo apt-get install openjdk-11-jdk
    $ sudo update-alternatives --config java
    # select the right one from the list

    # or 
    $ sudo update-java-alternatives --set /path/to/java/version
```

2. set the version for javac (compiler)  
```
    $ sudo update-alternatives --config javac
    # select the right one from the list

    # or 
    $ sudo update-java-alternatives --set /path/to/java/version
```

### Setup vscode workspace
Write the file: ./wsp.code-workspace
```
{
	"folders": [
		{
			"path": "."
		}
	],
	"settings": {
		"java.jdt.ls.java.home": "/usr/lib/jvm/java-11-openjdk-amd64",
		"java.configuration.updateBuildConfiguration": "automatic"
	}
}
```

### Notes 
- java dependencies are found in:  
    ```./libs```
- the dependency ```./libs/jniLibs_x64.jar``` is made of linux *.so library used 
    on runtime

## Code
After the development setup is complete, you are ready to go.  
The java source file is in ```./src/main/java/org/brainflow```

## Build
```
./gradlew clean build
```

## Run
Use Gradle task
```
./gradlew run
```
Use the built jar file
```
java -jar ./build/libs/brainflow_sandbox.jar
```

## License
[MIT](LICENSE)

## Contributing
[How to](CONTRIBUTING.md)

## Code of conduct
[Code of conduct](CODE_OF_CONDUCT.md)

## Credits
[Andrey Parfenov](https://github.com/Andrey1994)

## Brainflow
[website](https://brainflow.org/)  
[releases](https://github.com/brainflow-dev/brainflow/releases)  
[docs](https://brainflow.readthedocs.io/en/stable/)  